﻿using System;

namespace PatronBluider
{
    class Program
    {
        static void Main(string[] args)
        {
            Cocina  cocina = new Cocina();

            // un cliente pide una Pizza cuatro quesos familiar
            cocina.RecepcionarProximaPizza(new CuatroQuesosBuilder("Familiar"));
            cocina.CocinarPizzaPasoAPaso();
            var pizzaCuatroQuesos = cocina.PizzaPreparada;


            // otro cliente pide una Hawaiana
            cocina.RecepcionarProximaPizza(new HawaianaBuilder("Mediana"));
            cocina.CocinarPizzaPasoAPaso();
            var pizzaHawaiana = cocina.PizzaPreparada;

            Console.WriteLine(cocina);
            Console.ReadLine();
        }
    }
}
